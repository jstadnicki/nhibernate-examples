﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate;

namespace nhibernate.domain.test
{
    [TestFixture]
    public class ProductRepository_Fixture
    {
        private Configuration configuration;
        private NHibernate.ISessionFactory sessionFactory;

        private readonly Product[] products = new[]
            {
                new Product{Name="Melon", Category="Fruits"},
                new Product{Name="Pear", Category="Fruits"},
                new Product{Name="Milk", Category="Beverages"},
                new Product{Name="Coca cola", Category="Beverages"},
                new Product{Name="Pepsi Cola", Category="Beverages"},
            };

        private void CreateInitialDBContent()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var p in this.products)
                    {
                        session.Save(p);
                    }
                    transaction.Commit();
                }
            }

        }


        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            this.configuration = new Configuration();
            this.configuration.Configure();
            this.configuration.AddAssembly(typeof(Product).Assembly);
            this.sessionFactory = configuration.BuildSessionFactory();
        }

        [SetUp]
        public void SetupContext()
        {
            new SchemaExport(this.configuration).Execute(false, true, false);
            this.CreateInitialDBContent();
        }

        [Test]
        public void Can_Add_New_Product()
        {
            // arrange
            var product = new Product { Name = "Apple", Category = "Fruits" };
            var repository = new ProductRepository();
            
            // act
            repository.Add(product);

            using (ISession s = this.sessionFactory.OpenSession())
            {
                var fromdb = s.Get<Product>(product.Id);
                Assert.IsNotNull(fromdb);
                Assert.AreNotSame(product, fromdb);
                Assert.AreEqual(product.Name, fromdb.Name);
                Assert.AreEqual(product.Category, fromdb.Category);
            }
        }

        [Test]
        public void Can_Update_Existing_Product()
        {
            var p = this.products[0];
            p.Name = "Yellow Pear";
            var repo = new ProductRepository();
            repo.Update(p);

            using (var session = this.sessionFactory.OpenSession())
            {
                var fromdb = session.Get<Product>(p.Id);
                Assert.AreEqual(p.Name, fromdb.Name);
            }
        }

        [Test]
        public void Can_Remove_Existing()
        {
            var p = this.products[0];

            var repo = new ProductRepository();
            repo.Remove(p);

            using (var s = this.sessionFactory.OpenSession())
            {
                var fromdb = s.Get<Product>(p.Id);
                Assert.IsNull(fromdb);
            }
        }

        [Test]
        public void Can_Get_Using_Id()
        {
            var p = this.products[1];

            var repo = new ProductRepository();
            var result = repo.GetById(p.Id);

            Assert.AreEqual(result.Id, p.Id);
            Assert.AreEqual(result.Name, p.Name);
            Assert.AreEqual(result.Category, p.Category);

        }

        [Test]
        public void Can_Get_Using_Name()
        {
            var p = this.products[2];

            var repo = new ProductRepository();
            var result = repo.GetByName(p.Name);

            Assert.AreEqual(result.Id, p.Id);
            Assert.AreEqual(result.Name, p.Name);
            Assert.AreEqual(result.Category, p.Category);

        }


        [Test]
        public void Can_Get_Using_Category()
        {
            //var p = this.products[2];

            var repo = new ProductRepository();
            var result = repo.GetByCategory("Fruits");

            Assert.AreEqual(2, result.Count);
            Assert.IsTrue(IsInCollection(this.products[0], result));
            Assert.IsTrue(IsInCollection(this.products[1], result));

        }

        private bool IsInCollection(Product product, ICollection<Product> result)
        {
            foreach (var item in result)
            {
                if (item.Id == product.Id)
                {
                    return true;
                }
            }
            return false;
        }


       
    }
}
