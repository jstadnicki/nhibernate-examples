﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace nhibernate.psql.domain
{
    public interface IProductRepository
    {
        void Add(Product p);
        void Update(Product p);
        void Remove(Product p);
        Product GetById(Guid id);
        Product GetByName(string name);
        ICollection<Product> GetByCategory(string category);
    }
}
