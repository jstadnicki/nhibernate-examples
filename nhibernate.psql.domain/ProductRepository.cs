using System;
using System.Collections.Generic;
using NHibernate.Criterion;

namespace nhibernate.psql.domain
{
    public class ProductRepository : IProductRepository
    {
        public void Add(Product p)
        {
            using (var s = NHiberanteHelper.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    s.Save(p);
                    t.Commit();
                }
            }
        }

        public void Update(Product p)
        {
            using (var s = NHiberanteHelper.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    s.Update(p);
                    t.Commit();
                }
            }
        }

        public void Remove(Product p)
        {
            using (var s = NHiberanteHelper.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    s.Delete(p);
                    t.Commit();
                }
            }
        }

        public Product GetById(Guid id)
        {
            using (var s = NHiberanteHelper.OpenSession())
            {
                return s.Get<Product>(id);
            }
        }

        public Product GetByName(string name)
        {
            using (var s = NHiberanteHelper.OpenSession())
            {
                Product p = s.CreateCriteria(typeof(Product))
                    .Add(Restrictions.Eq("Name", name))
                    .UniqueResult<Product>();

                return p;
            }
        }

        public ICollection<Product> GetByCategory(string category)
        {

            using (var s = NHiberanteHelper.OpenSession())
            {
                var p = s.CreateCriteria(typeof(Product))
                    .Add(Restrictions.Eq("Category", category))
                    .List<Product>();

                return p;
            }

        }
    }
}