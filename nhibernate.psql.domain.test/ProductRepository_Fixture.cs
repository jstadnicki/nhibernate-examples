﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace nhibernate.psql.domain.test
{
    [TestFixture]
    public class ProductRepository_Fixture
    {
        private Configuration configuration;
        private ISessionFactory sessionFactory;

        private readonly Product[] products = new[]
            {
                new Product{Name="Melon", Category="Fruits"},
                new Product{Name="Pear", Category="Fruits"},
                new Product{Name="Milk", Category="Beverages"},
                new Product{Name="Coca cola", Category="Beverages"},
                new Product{Name="Pepsi Cola", Category="Beverages"},
            };

        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            this.configuration = new Configuration();
            this.configuration.Configure();
            this.configuration.AddAssembly(typeof(Product).Assembly);
            this.sessionFactory = this.configuration.BuildSessionFactory();
        }

        [SetUp]
        public void SetupContext()
        {
            new SchemaExport(this.configuration).Execute(false, true, false);
            this.CreateInitialDBContent();
        }

        private void CreateInitialDBContent()
        {
            using (var session = this.sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    foreach (var p in this.products)
                    {
                        session.Save(p);
                    }
                    transaction.Commit();
                }
            }
        }



        [Test]
        public void Can_Add_A_New_Product()
        {
            // arrange
            var p = new Product { Name = "PineApple", Category = "Fruits" };
            var repo = new ProductRepository();

            // act            
            repo.Add(p);

            // assert
            using (var s = this.sessionFactory.OpenSession())
            {
                var fromdb = s.Get<Product>(p.Id);
                Assert.IsNotNull(fromdb);
                Assert.AreNotSame(fromdb, p);
                Assert.AreEqual(fromdb.Name, p.Name);
                Assert.AreEqual(fromdb.Category, p.Category);
            }
        }

        [Test]
        public void Can_Update_Existing_Product()
        {
            // arrange
            var p = this.products[1];
            p.Name = "changed";
            var repo = new ProductRepository();

            // act
            repo.Update(p);

            // assert
            using (var s = this.sessionFactory.OpenSession())
            {
                var res = s.Get<Product>(p.Id);
                Assert.AreEqual(res.Name, p.Name);
            }
        }

        [Test]
        public void Can_Remove_Existing()
        {
            // arrange
            var p = this.products[1];
            var repo = new ProductRepository();

            // act
            repo.Remove(p);

            // assert
            using (var s = this.sessionFactory.OpenSession())
            {
                var res = s.Get<Product>(p.Id);
                Assert.IsNull(res);
            }
        }


        [Test]
        public void Can_Get_By_Name()
        {
            // arrange
            var p = this.products[2];
            var repo = new ProductRepository();

            // act
            var res = repo.GetByName(p.Name);

            // assert
            Assert.AreEqual(res.Id, p.Id);
            Assert.AreEqual(res.Name, p.Name);
            Assert.AreEqual(res.Category, p.Category);
        }

        [Test]
        public void Can_Get_By_Id()
        {
            // arrange
            var p = this.products[3];
            var repo = new ProductRepository();

            // act
            var res = repo.GetById(p.Id);

            // assert
            Assert.AreEqual(res.Id, p.Id);
            Assert.AreEqual(res.Name, p.Name);
            Assert.AreEqual(res.Category, p.Category);
        }

        [Test]
        public void Can_Get_By_Categories()
        {
            // arrange
            var p = this.products[3];
            var repo = new ProductRepository();

            // act
            var res = repo.GetByCategory(p.Category);

            // assert
            Assert.AreEqual(res.Count, 3);
            Assert.IsTrue(IsInCollection(this.products[2], res));
            Assert.IsTrue(IsInCollection(this.products[3], res));
            Assert.IsTrue(IsInCollection(this.products[4], res));
        }

        private bool IsInCollection(Product products, ICollection<Product> res)
        {
            foreach (var p in res)
            {
                if (products.Id == p.Id)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
