﻿using System;
using System.Linq;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace nhibernate.psql.domain.test
{
    [TestFixture]
    public class GenerateSchema_Fixture
    {
        [Test]
        public void Can_Generate_Schema()
        {
            var cfg = new Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Product).Assembly);

            new SchemaExport(cfg).Execute(false, true, false);
        }
    }
}
